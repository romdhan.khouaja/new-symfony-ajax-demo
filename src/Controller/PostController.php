<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


use App\Entity\Post;
use App\Repository\PostRepository;
use App\Entity\PostLike;
use App\Repository\PostLikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class PostController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(PostRepository $repo)
    {
        return $this->render('post/index.html.twig', [
            'posts' => $repo->findAll(),
        ]);
    }
    /**
     * permet de liker ou de unliker un article
     * 
     * @Route("/post/{id}/like", name="post_json_like")
     */
    public function like(SerializerInterface $serializer, Post $post, EntityManagerInterface $manager, PostLikeRepository $postLikeRepository): Response
    {
        $user = $this->getUser();

        if (!$user) {
            return  $this->json([
                'code' => 403,
                'message' => 'retour non concluant like en json'
            ], 403);
        }

        if ($post->isLikedByUser($user)) {
            $like = $postLikeRepository->findOneBy(['post' => $post, 'user' => $user]);
            $manager->remove($like);
            $manager->flush();

            $json = $serializer->serialize(
                $postLikeRepository->count(['post' => $post]),
                'json',
                ['groups' => 'show_post']
            );
            return $this->json([
                'code' => 200,
                'message' => 'suppression avec succes !',
                'icone' => 'far',
                'likes' => $json
            ], 200);
        }

        $like = new PostLike();
        $like->setPost($post)->setUser($user);
        $manager->persist($like);
        $manager->flush();
        $json = $serializer->serialize(
            $postLikeRepository->count(['post' => $post]),
            'json',
            ['groups' => 'show_post']
        );
        return $this->json([
            'code' => 200,
            'message' => 'ajout avec succes !',
            'icone' => 'fas',
            'likes' => $json
        ], 200);
    }
}
